import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Tugas11 from './tugas11/tugas11';
import Tugas12 from './tugas12/tugas12';
import Tugas13 from './tugas13/tugas13';
import Tugas14 from './tugas14/tugas14';
import Tugas15 from './tugas15/App';
function App() {
  return (
    <div className="App">
        <Router>
          <div>
            <nav>
              <ul>
                <li>
                  <Link to="/tugas11">tugas11</Link>
                </li>
                <li>
                  <Link to="/tugas12">tugas12</Link>
                </li>
                <li>
                  <Link to="/tugas13">tugas13</Link>
                </li>
                <li>
                  <Link to="/tugas14">tugas14</Link>
                </li>
                <li>
                  <Link to="/">tugas15</Link>
                </li>
              </ul>
            </nav>
            <Switch>
              <Route path="/tugas11">
                <Tugas11_ />
              </Route>
              <Route path="/tugas12">
                <Tugas12_ />
              </Route>
              <Route path="/tugas13">
                <Tugas13_ />
              </Route>
              <Route path="/tugas14">
                <Tugas14_ />
              </Route>
              <Route path="/">
                <Tugas15_ />
              </Route>
            </Switch>
        </div>
      </Router>
    </div>
  );
}


function Tugas15_() {
  return <Tugas15 />;
}

function Tugas14_() {
  return <Tugas14 />;
}

function Tugas13_() {
  return <Tugas13 />;
}

function Tugas12_() {
  return <Tugas12 />;
}

function Tugas11_() {
  return <Tugas11 />;
}

export default App;
