import React, {useContext,useState} from "react"
import axios from "axios";
import {DaftarHargaBuahcontext} from "./DaftarHargaBuahcontext"

const DaftarHargaBuahlist = () =>{
    const { value1, value2,value3,value4,value5,value6 } = React.useContext(DaftarHargaBuahcontext);  
  const [dataHargaBuah, setdataHargaBuah] = value1
  const [nama, setnama]  =  value2
  const [harga, setharga]  =  value3
  const [berat, setberat]  =  value4
  const [indexOfForm, setIndexOfForm] =  value5
  const [id, setid] =  value6

  const handleEdit = (event) =>{
    let index = event.target.value;
    let indexarr = event.target.getAttribute('data-indexarr')
    axios.get('http://backendexample.sanbercloud.com/api/fruits/'+index)
    .then(res => {
        setnama(res.data.name)
        setharga(res.data.price)
        setberat(res.data.weight)
        setIndexOfForm(indexarr)
        setid(res.data.id)
    })

    
  }



const handleDelete = (event) =>{
    let index = event.target.value;
    let indexarr = event.target.getAttribute('data-indexarr')
    let newdataHargaBuah = dataHargaBuah

    axios.delete('http://backendexample.sanbercloud.com/api/fruits/'+index)
    .then(res => {
        newdataHargaBuah.splice(indexarr,1)
        setdataHargaBuah([...newdataHargaBuah])
    })
    
  } 
    
  return(
      <>
        <h2><b>Tabel Harga Buah</b></h2>
            <table className="center">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                {dataHargaBuah !== null && dataHargaBuah.map((val,index)=> {
                    return (
                        <tr key={index}> 
                            <td>{index+1}</td>
                            <td>{val.nama}</td>
                            <td>{val.harga}</td>
                            <td>{val.berat / 1000} Kg</td>
                            <td>
                                <button onClick={handleEdit} value={val.id} data-indexarr={index}>Edit</button>
                                &nbsp;
                                <button onClick={handleDelete} value={val.id} data-indexarr={index}>Delete</button>
                            </td>
                        </tr>
                    )
                    })}
            
            </tbody>
        </table>
      </>
  )

}

export default DaftarHargaBuahlist