import React, {useContext, useState} from "react"
import axios from "axios";
import {DaftarHargaBuahcontext} from "./DaftarHargaBuahcontext"

const DaftarHargaBuahform = () =>{
    const { value1, value2,value3,value4,value5,value6 } = React.useContext(DaftarHargaBuahcontext);  
  const [dataHargaBuah, setdataHargaBuah] = value1
  const [nama, setnama]  =  value2
  const [harga, setharga]  =  value3
  const [berat, setberat]  =  value4
  const [indexOfForm, setIndexOfForm] =  value5
  const [id, setid] =  value6

    const handleChangeNama = (event) =>{
        setnama(event.target.value);
      }

      const handleChangeHarga = (event) =>{
        setharga(event.target.value);
      }

      const handleChangeBerat = (event) =>{
        setberat(event.target.value);
      }

      

    

    const handleSubmit = (event) =>{
        event.preventDefault()
        let newdataHargaBuah = dataHargaBuah
        if(indexOfForm  === -1){
            newdataHargaBuah = [...newdataHargaBuah, {id:indexOfForm,nama:nama,harga:harga,berat:berat}]
            axios.post('http://backendexample.sanbercloud.com/api/fruits', {name:nama,price:harga,weight:berat})
            .then(res => {
                console.log(res);
            })
        }else{
            console.log(id)
            axios.post('http://backendexample.sanbercloud.com/api/fruits', {id:id,name:nama,price:harga,weight:berat})
            .then(res => {
                console.log(res);
            })
            newdataHargaBuah[indexOfForm] =  {id:indexOfForm,nama:nama,harga:harga,berat:berat}
            newdataHargaBuah = newdataHargaBuah
        }

        
      

        setdataHargaBuah(newdataHargaBuah)
        setnama("")
        setharga("")
        setberat("")
      }

  return(
    <>
      <h1>Form Peserta</h1>
        <form onSubmit={handleSubmit}>
          <div>
          <label className="paddingright">
            Masukkan nama buah:
          </label>          
          <input type="text" name="nama" value={nama} onChange={handleChangeNama}/>
          </div>
          <div>
          <label className="paddingright">
            Masukkan Harga buah: 
          </label> 
           <input type="number" name="harga" value={harga} onChange={handleChangeHarga}/>
          </div>
          <div>
          <label className="paddingright">
            Masukkan Berat buah:
          </label> 
          <input type="number" name="buah"  value={berat} onChange={handleChangeBerat}/>
          </div>
          <button>submit</button>
        </form>
    </>
  )

}

export default DaftarHargaBuahform