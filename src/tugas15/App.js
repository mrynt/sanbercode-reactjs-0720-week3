import React from "react"
import {DaftarHargaBuahProvider} from "./DaftarHargaBuahcontext"
import DaftarHargaBuahlist from "./DaftarHargaBuahlist"
import DaftarHargaBuahform from "./DaftarHargaBuahform"

const App = () =>{
  return(
    <DaftarHargaBuahProvider>
      <DaftarHargaBuahlist/>
      <DaftarHargaBuahform/>
    </DaftarHargaBuahProvider>
  )
}

export default App