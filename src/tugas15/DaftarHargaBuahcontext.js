import React, { useState, createContext,useEffect } from "react";
import axios from "axios";
export const DaftarHargaBuahcontext = createContext();

export const DaftarHargaBuahProvider = props => {
    const [dataHargaBuah, setdataHargaBuah] =  useState(null)
    const [nama, setnama]  =  useState("")
    const [harga, setharga]  =  useState("")
    const [berat, setberat]  =  useState("")
    const [indexOfForm, setIndexOfForm] =  useState(-1)
    const [id, setid] =  useState(-1)  


    useEffect( () => {
        if(dataHargaBuah ===null){
            axios.get('http://backendexample.sanbercloud.com/api/fruits')
            .then(res => {
                setdataHargaBuah(res.data.map(el => {return {id:el.id,nama:el.name,harga:el.price,berat:el.weight}}))
            })
        }
       
      })

  return (
    <DaftarHargaBuahcontext.Provider value={{value1:[dataHargaBuah, setdataHargaBuah],
      value2:[nama, setnama],
      value3 :[harga, setharga],
      value4 :[berat, setberat],
      value5:[indexOfForm, setIndexOfForm],
      value6:[id, setid]
                                            }}>
      {props.children}
    </DaftarHargaBuahcontext.Provider>
  );
};