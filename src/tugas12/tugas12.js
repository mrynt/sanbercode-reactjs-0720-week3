import React, {Component} from 'react'

class Clock extends Component {
    constructor(props) {
      super(props);
      this.state = {
        time: new Date().toLocaleTimeString()
      };
    }
    componentDidMount() {
      this.intervalID = setInterval(
        () => this.tick(),
        1000
      );
    }
    componentWillUnmount() {
      clearInterval(this.intervalID);
    }
    tick() {
      this.setState({
        time: new Date().toLocaleTimeString()
      });
    }
    render() {
      return (
        <h1 className="left">
          Sekarang jam : {this.state.time}
        </h1>
      );
    }
  }

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 100
    }
  }

  componentDidMount(){
    console.log('Component DID MOUNT!')
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount(){
    console.log('componentWillUnmount!')
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1 
    });
  }


  render(){
    return(
      <>
        {this.state.time >= 1 &&
        <div className="blocktugas12">
        <Clock />
        <h1 className="right">
          Hitung Mundur : {this.state.time}
        </h1>
        </div>
        }
      </>
    )
  }
}

export default Timer