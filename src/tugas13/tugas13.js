import React from 'react';

class Component extends React.Component {
    constructor(props){
        super(props)
        this.state ={
            dataHargaBuah :  [
            {nama: "Semangka", harga: 10000, berat: 1000},
            {nama: "Anggur", harga: 40000, berat: 500},
            {nama: "Strawberry", harga: 30000, berat: 400},
            {nama: "Jeruk", harga: 30000, berat: 1000},
            {nama: "Mangga", harga: 30000, berat: 500}
          ],
          obj : {}  ,
         nama:""  ,
         harga:"",
         berat:"",
         indexOfForm:-1
        }
    
        this.handleChangeNama = this.handleChangeNama.bind(this);
        this.handleChangeHarga = this.handleChangeHarga.bind(this);
        this.handleChangeBerat = this.handleChangeBerat.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
      }
    
    handleChangeNama(event){
        this.setState({nama: event.target.value});
      }

    handleChangeHarga(event){
        this.setState({harga: event.target.value});
    }

    handleChangeBerat(event){
      this.setState({berat: event.target.value});
    }

    handleEdit(event){
        let index = event.target.value;
        let data = this.state.dataHargaBuah[index]
        this.setState({
            nama: data.nama,
            harga: data.harga,
            berat: data.berat,
            indexOfForm:index
        })

    }


    handleDelete(event){
        let index = event.target.value;
        let newdataHargaBuah = this.state.dataHargaBuah
        newdataHargaBuah.splice(index,1)
        this.setState({
            dataHargaBuah: newdataHargaBuah
        })
    }
    
    handleSubmit(event){
        event.preventDefault()
        let nama = this.state.nama;
        let harga = this.state.harga;
        let berat = this.state.berat;
        this.state.obj = {nama,harga,berat}


        let dataHargaBuah = this.state.dataHargaBuah

        if(this.state.indexOfForm === -1){
            dataHargaBuah = [...this.state.dataHargaBuah, this.state.obj]
        }else{
            dataHargaBuah[this.state.indexOfForm] = this.state.obj
            dataHargaBuah = dataHargaBuah
        }
      

        this.setState({
            dataHargaBuah: dataHargaBuah,
            nama: "",
            harga: "",
            berat: ""
        })
      }

  render() {
    return (
        <>
        <h1>Form Peserta</h1>
        <form onSubmit={this.handleSubmit}>
          <div>
          <label className="paddingright">
            Masukkan nama buah:
          </label>          
          <input type="text" name="nama" value={this.state.nama} onChange={this.handleChangeNama}/>
          </div>
          <div>
          <label className="paddingright">
            Masukkan Harga buah: 
          </label> 
           <input type="number" name="harga" value={this.state.harga} onChange={this.handleChangeHarga}/>
          </div>
          <div>
          <label className="paddingright">
            Masukkan Berat buah:
          </label> 
          <input type="number" name="buah"  value={this.state.berat} onChange={this.handleChangeBerat}/>
          </div>
          <button>submit</button>
        </form>
        <h2><b>Tabel Harga Buah</b></h2>
        <table className="center">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Berat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            {this.state.dataHargaBuah.map((val,index)=> {
                return (
                    <tr key={index}> 
                        <td>{index+1}</td>
                        <td>{val.nama}</td>
                        <td>{val.harga}</td>
                        <td>{val.berat / 1000} Kg</td>
                        <td>
                            <button onClick={this.handleEdit} value={index}>Edit</button>
                            &nbsp;
                            <button onClick={this.handleDelete} value={index}>Delete</button>
                        </td>
                    </tr>
                )
                })}
           
        </tbody>
      </table>
      </>
    )
  }
}

export default Component