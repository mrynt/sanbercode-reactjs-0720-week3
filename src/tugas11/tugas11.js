import React from 'react';

class Tbody extends React.Component {
  render() {
    return <tr> 
                <td>{this.props.nama}</td>
                <td>{this.props.harga}</td>
                <td>{this.props.berat / 1000} Kg</td>
            </tr>;
  }
}


let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]
 
class Component extends React.Component {
  render() {
    return (
        <>
        <h2><b>Tabel Harga Buah</b></h2>
        <table className="center">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Berat</th>
                </tr>
            </thead>
            <tbody>
            {dataHargaBuah.map(el=> {
                return (
                    <Tbody nama={el.nama} harga={el.harga} berat={el.berat}  key={el.nama}/>
                )
                })}
           
        </tbody>
      </table>
      </>
    )
  }
}

export default Component