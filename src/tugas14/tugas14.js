import React, {useState,useEffect} from 'react';
import axios from "axios";
const Component = () => {

    const [dataHargaBuah, setdataHargaBuah] =  useState(null)

    const [nama, setnama]  =  useState("")
    const [harga, setharga]  =  useState("")
    const [berat, setberat]  =  useState("")
    const [indexOfForm, setIndexOfForm] =  useState(-1)
    const [id, setid] =  useState(-1)  
    
    useEffect( () => {
        if(dataHargaBuah ===null){
            axios.get('http://backendexample.sanbercloud.com/api/fruits')
            .then(res => {
                setdataHargaBuah(res.data.map(el => {return {id:el.id,nama:el.name,harga:el.price,berat:el.weight}}))
            })
        }
       
      })

    const handleChangeNama = (event) =>{
        setnama(event.target.value);
      }

      const handleChangeHarga = (event) =>{
        setharga(event.target.value);
      }

      const handleChangeBerat = (event) =>{
        setberat(event.target.value);
      }

      const handleEdit = (event) =>{
        let index = event.target.value;
        let indexarr = event.target.getAttribute('data-indexarr')
        axios.get('http://backendexample.sanbercloud.com/api/fruits/'+index)
        .then(res => {
            setnama(res.data.name)
            setharga(res.data.price)
            setberat(res.data.weight)
            setIndexOfForm(indexarr)
            setid(res.data.id)
        })

        
      }

    

    const handleDelete = (event) =>{
        let index = event.target.value;
        let indexarr = event.target.getAttribute('data-indexarr')
        let newdataHargaBuah = dataHargaBuah

        axios.delete('http://backendexample.sanbercloud.com/api/fruits/'+index)
        .then(res => {
            newdataHargaBuah.splice(indexarr,1)
            setdataHargaBuah([...newdataHargaBuah])
        })
        
      }

    

    const handleSubmit = (event) =>{
        event.preventDefault()
        let newdataHargaBuah = dataHargaBuah
        console.log(dataHargaBuah)
        console.log(indexOfForm)
        if(indexOfForm  === -1){
            newdataHargaBuah = [...newdataHargaBuah, {id:indexOfForm,nama:nama,harga:harga,berat:berat}]
            axios.post('http://backendexample.sanbercloud.com/api/fruits', {name:nama,price:harga,weight:berat})
            .then(res => {
                console.log(res);
            })
        }else{
            console.log(id)
            axios.post('http://backendexample.sanbercloud.com/api/fruits', {id:id,name:nama,price:harga,weight:berat})
            .then(res => {
                console.log(res);
            })
            newdataHargaBuah[indexOfForm] =  {id:indexOfForm,nama:nama,harga:harga,berat:berat}
            newdataHargaBuah = newdataHargaBuah
        }

        
      

        setdataHargaBuah(newdataHargaBuah)
        setnama("")
        setharga("")
        setberat("")
      }


      

    return (
        <>
        <h1>Form Peserta</h1>
        <form onSubmit={handleSubmit}>
          <div>
          <label className="paddingright">
            Masukkan nama buah:
          </label>          
          <input type="text" name="nama" value={nama} onChange={handleChangeNama}/>
          </div>
          <div>
          <label className="paddingright">
            Masukkan Harga buah: 
          </label> 
           <input type="number" name="harga" value={harga} onChange={handleChangeHarga}/>
          </div>
          <div>
          <label className="paddingright">
            Masukkan Berat buah:
          </label> 
          <input type="number" name="buah"  value={berat} onChange={handleChangeBerat}/>
          </div>
          <button>submit</button>
        </form>
        <h2><b>Tabel Harga Buah</b></h2>
        <table className="center">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Berat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            {dataHargaBuah !== null && dataHargaBuah.map((val,index)=> {
                return (
                    <tr key={index}> 
                        <td>{index+1}</td>
                        <td>{val.nama}</td>
                        <td>{val.harga}</td>
                        <td>{val.berat / 1000} Kg</td>
                        <td>
                            <button onClick={handleEdit} value={val.id} data-indexarr={index}>Edit</button>
                            &nbsp;
                            <button onClick={handleDelete} value={val.id} data-indexarr={index}>Delete</button>
                        </td>
                    </tr>
                )
                })}
           
        </tbody>
      </table>
      </>
    )

}

export default Component